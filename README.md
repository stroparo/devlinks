# Dev Links

Links starting with "./" are subpages in this repository itself.

* code styles - [dotnet](https://github.com/dotnet/runtime/blob/master/docs/coding-guidelines/coding-style.md) - [groovy](http://groovy-lang.org/style-guide.html) - [python](https://pep8.org/) - [ruby](https://rubystyle.guide/) - [@google](https://google.github.io/styleguide/)
    - [javascript](https://javascript.info/coding-style) - [@flaviocopes](https://flaviocopes.com/javascript-coding-style/)/[AirBNB](https://github.com/airbnb/javascript) - [idiomatic.js](https://github.com/rwaldron/idiomatic.js) - [StandardJS](https://standardjs.com/)(+tool)

---
